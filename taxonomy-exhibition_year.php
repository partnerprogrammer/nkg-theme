<?php
  
  get_header();

    _partial('_wrap-start');
      
      _partial('_nav-tax-year');
      _partial('_pagetitle-tax');

      echo '<div class="row">';
        if ( have_posts() ) {
          while ( have_posts() ) {
            the_post();
            echo '<div class="col-12 d-flex">';
              _loop('loop-exhibition');
            echo '</div>';
          }
          if (function_exists('wp_pagenavi')) { wp_pagenavi(); };

          show_next_term_link(
            get_terms(['taxonomy' => 'exhibition_year', 'hide_empty' => false ]),
            'exhibitions'
          );

          wp_reset_postdata();
        } else {
          _content('content-none');
        }
      echo '</div>';

    _partial('_wrap-end');
  
    echo '<style>.menu-item-object-exhibitions a {color: #2e37fe !important; }</style>';
  get_footer();