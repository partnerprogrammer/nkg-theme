<?php

  get_header();

    _partial('_wrap-start');
      // _partial('_h-page');

      if ((is_home() && !is_page()) || is_category()) :
        _content('content-index');

      elseif (is_singular()) :
        
        $template = locate_template('contents/content-single-' . $post->post_type . '.php');
        
        if ($template) {          
          _content('content-single-' . $post->post_type);          
        } else {
          _content('content-single');
        }        

      elseif (is_404()) :
        _content('content-404');

      elseif (is_search()) :
        _content('content-search');
      
      else :
        $obj = is_archive() ? get_queried_object() : false;        
        $template = locate_template('contents/content-' . $obj->name . '.php');
                
        if ($obj && $template) {          
          _content('content-' . $obj->name);          
        } else {
          _content('content-index');
        }      

      endif;

    _partial('_wrap-end');
  
  get_footer();