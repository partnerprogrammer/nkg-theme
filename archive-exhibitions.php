<?php
  $terms = get_terms(['taxonomy' => 'exhibition_year', 'hide_empty' => false ]);
  foreach ($terms as $term ) {
    if ($term->slug != 'upcoming') {
      wp_safe_redirect( get_term_link($term), 308 );
      exit();
    }
  }
?>