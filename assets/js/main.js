// !function(e,s,t){e(function(){"use strict";e("#fe-home").slick({arrows:!0,infinite:!0,fade:!0,speed:500,slidesToShow:1,slidesToScroll:1,autoplaySpeed:5e3,rows:0,appendArrows:e("#fe-home-navigation .arrows"),responsive:[{breakpoint:768,settings:{dots:!0,arrows:!1}}]}),e(".slider-news").slick({dots:!0,arrows:!1,rows:0,infinite:!1,appendArrows:e(".block-news .block-header-navigation"),responsive:[{breakpoint:9999,settings:{variableWidth:!0,slidesToShow:1,arrows:!0,adaptiveHeight:!1}},{breakpoint:992,settings:{slidesToShow:1,variableWidth:!0,adaptiveHeight:!0}}]});stickybits(".sticky",{stickyBitStickyOffset:150});e(".artists-list").length>0&&e(".artists-list a").hover(function(){var s;s=e(this).parent("li"),e(".artists-list li").removeClass("active"),s.addClass("active"),function(s){e(".artists-thumb figure").remove();var i=s.data("thumb"),a=s.data("caption");if(i!==t&&i.length>0){var o=e(`<figure><div class="img-wrapper"><img src="${i}" title="${e.trim(s.text())}" /></div><figcaption>${a}</figcaption></figure>`).css("display","flex").hide().fadeIn(700);e(".artists-thumb").append(o)}}(e(this))},function(){}),e.each(e(".slick-artworks"),function(s,t){var i=e(t),a=i.closest(".block-artworks").find(".block-header-navigation");i.slick({appendArrows:a,dots:!1,arrows:!0,adaptiveHeight:!0,variableWidth:!0,infinite:!0,rows:0,responsive:[{breakpoint:1200,settings:{slidesToShow:3,slidesToScroll:1}},{breakpoint:992,settings:{slidesToShow:2,slidesToScroll:1,dots:!0}},{breakpoint:768,settings:{slidesToShow:1,slidesToScroll:1,variableWidth:!1}}]})}),e(".slick-publications").slick({rows:0,arrows:!0,slidesToShow:1,slidesToScroll:1,responsive:[{breakpoint:768,settings:{slidesToShow:1,slidesToScroll:1,dots:!0,arrows:!1,adaptiveHeight:!0}}]}),e(".wpcf7-select").length&&e.each(e(".wpcf7-select"),function(s,t){window["select"+t.name]=new Choices(t),window["select"+t.name].passedElement.element.addEventListener("showDropdown",function(s){e('[name="'+t.name+'"]').closest(".wpcf7-form-control-wrap").siblings("label").addClass("has-value")},!1),window["select"+t.name].passedElement.element.addEventListener("hideDropdown",function(s){window["select"+t.name].getValue().length||e('[name="'+t.name+'"]').closest(".wpcf7-form-control-wrap").siblings("label").removeClass("has-value")},!1)}),e(".js-navigate-onchange").on("change",function(e){window.location=e.target.value}),e(document).on("click","[data-href]",function(){window.location=e(this).data("href")});var s=document.querySelector(".wpcf7");s.addEventListener("wpcf7submit",function(s){e(".wpcf7-form-control-wrap").removeClass("wpcf7-not-valid"),e("select.wpcf7-not-valid").closest(".wpcf7-form-control-wrap").addClass("wpcf7-not-valid")},!1),s.addEventListener("wpcf7mailsent",function(s){e("label.has-value").removeClass("has-value")},!1),e(".buy-domestic-shipping").on("click",function(s){console.log("a"),s.preventDefault();var t=e(this).siblings("form")[0];e(t).attr("target","_blank"),t.submit()}),e(".buy-international-shipping").on("click",function(s){console.log("b"),s.preventDefault();var t=e(this).siblings("form")[1];e(t).attr("target","_blank"),t.submit()}),e("#search-box input, .wpcf7-form input, .wpcf7-form textarea").focus(function(){e(this).parent().siblings("label").addClass("has-value"),e(this).addClass("val")}).blur(function(){""===e(this).val()&&(e(this).parent().siblings("label").removeClass("has-value"),e(this).removeClass("val"))}),e(".wpcf7-form .date").blur(function(){0===e(this).val().length&&e(this).addClass("not-value")}),e("select").not(".multiple").wrap('<div class="select-box"></div>'),e().fancybox({selector:".slick-artworks .slick-slide:not(.slick-cloned) figure a",backFocus:!1,afterShow:function(e,s){s.opts.$orig.closest(".slick-initialized").slick("slickGoTo",parseInt(s.index),!0)}}),e("a.scroll, li.scroll a").click(function(){if(location.pathname.replace(/^\//,"")==this.pathname.replace(/^\//,"")&&location.hostname==this.hostname){var s=e(this.hash);if((s=s.length?s:e("[name="+this.hash.slice(1)+"]")).length)return e("html,body").animate({scrollTop:s.offset().top},1e3),!1}})})}(jQuery);

(function ($, root, undefined) {
	$(function () {

		'use strict';

	  // banner
	  $('#fe-home').slick({
	    arrows: true,
			infinite: true,
			fade: true,
	    speed: 500,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    // autoplay: true,
      autoplaySpeed: 5000,
			rows: 0,
      appendArrows: $('#fe-home-navigation .arrows'),
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false
          }
        }
      ]
    });
    
    // news
    $('.slider-news').slick({
      dots: true,
      arrows: false,
      rows: 0,			
			infinite: false,
			appendArrows: $('.block-news .block-header-navigation'),
      responsive: [
        {
          breakpoint: 9999,
          settings: {
						variableWidth: true,
						slidesToShow: 1,
						arrows: true,
						adaptiveHeight: false,
          }
        },
        {
          breakpoint: 992,
          settings: {
						slidesToShow: 1,
						variableWidth: true,
						adaptiveHeight: true,
          }
        }
      ]
    });

		/* Artists Page */
		/* ----------------------------------------- */
			var sticky = stickybits('.sticky', { stickyBitStickyOffset: 150 });

			if ( $(".artists-list").length > 0 ) {

				$(".artists-list a")					
					.hover(
						function(){
							activateArtist($(this).parent('li'));
							activateArtistThumb($(this));						
						}, function() {
							// $(".artists-thumb img").remove(); 
						}
					);
			}
			
			function activateArtistThumb($el) {
				$(".artists-thumb figure").remove();
				var thumb = $el.data("thumb");
				var caption = $el.data("caption");
				if ( thumb !== undefined ) {
					if ( thumb.length > 0 ) {
						var image = $(`<figure><div class="img-wrapper"><img src="${thumb}" title="${$.trim($el.text())}" /></div><figcaption>${caption}</figcaption></figure>`).css('display', 'flex').hide().fadeIn(700);
						$('.artists-thumb').append(image);						
					}
				}
			}

			function activateArtist($el) {
				$('.artists-list li').removeClass('active');
				$el.addClass('active');
			}

		/* ----------------------------------------- Artists Page */
		
		/* Single Artist */
		/* ----------------------------------------- */
			
		$('.block-artworks-grid-2 .block-body').slick({
			dots: true,
			arrows: false,
			infinite: false,
			adaptiveHeight: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			rows: 1,
			responsive: [
				{
					breakpoint: 9999,
					settings: "unslick"
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				}
			]
		})

			// $.each($('.slick-artworks'), function (index, el) { 
			// 	 var $el = $(el);
			// 	 var $parent = $el.closest('.block-artworks');				 
			// 	 var $header_nav = $parent.find('.block-header-navigation');
				 
			// 	 $el.slick({
			// 		appendArrows: $header_nav,
			// 		dots: false,
			// 		arrows: true,
			// 		adaptiveHeight: true,
			// 		variableWidth: true,
			// 		infinite: true,
			// 		rows: 0,
			// 		responsive: [
			// 			{
			// 				breakpoint: 1200,
			// 				settings: {
			// 					slidesToShow: 3,
			// 					slidesToScroll: 1
			// 				}
			// 			},
			// 			{
			// 				breakpoint: 992,
			// 				settings: {
			// 					slidesToShow: 2,
			// 					slidesToScroll: 1,
			// 					dots: true,
			// 				}
			// 			},
			// 			{
			// 				breakpoint: 768,
			// 				settings: {
			// 					slidesToShow: 1,
			// 					slidesToScroll: 1,
			// 					variableWidth: false,
			// 				}
			// 			}
			// 		]
			// 	});

			// });			

			
			$('.slick-publications').slick({							
				rows: 0,	
				arrows: true,				
				// appendArrows: $('.block-publications .block-header-navigation'),
				slidesToShow: 1,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							dots: true,
							arrows: false,					
							adaptiveHeight: true,			
						}
					}
				]
      });
		/* ----------------------------------------- Single Artist */
		
		/* ChoicesJs */
		/* ----------------------------------------- */
			if ($('.wpcf7-select').length) {
				$.each($('.wpcf7-select'), function(index, value) {
					
					window['select' + value.name] = new Choices(value);
					window['select' + value.name].passedElement.element
						.addEventListener(
						'showDropdown',
						function(event) {
							$('[name="' + value.name + '"]').closest('.wpcf7-form-control-wrap').siblings('label').addClass('has-value');
						},
						false,
					);
					window['select' + value.name].passedElement.element
						.addEventListener(
						'hideDropdown',
						function(event) {
							var choices = window['select' + value.name].getValue();							
							if (!choices.length) {								
								$('[name="' + value.name + '"]').closest('.wpcf7-form-control-wrap').siblings('label').removeClass('has-value');
							}
						},
						false,
					);
				});
			}	
		/* ----------------------------------------- ChoicesJs */
		
		$('.js-navigate-onchange').on('change', function(e) {
			window.location = e.target.value;
		});

		$(document).on('click', '[data-href]', function() {			
			window.location = $(this).data('href');
		});
		
		/* Signup */
		/* ----------------------------------------- */

			$('.js-toggle-newsletter').click(function(e) {
				if (e.target.tagName === 'BUTTON') {
					$(e.target).addClass('d-none')					
				}				
				$('.js-toggle-newsletter-wrapper').toggleClass('d-none')
			})

			var wpcf7Elm = document.querySelector( '.wpcf7' );

			wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
				$('.wpcf7-form-control-wrap').removeClass('wpcf7-not-valid');
				$('select.wpcf7-not-valid').closest('.wpcf7-form-control-wrap').addClass('wpcf7-not-valid');
			}, false );

			
			wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
				$('label.has-value').removeClass('has-value');
			}, false );


		/* ----------------------------------------- Signup */
		
		/* Buy Button	 */
        /* ----------------------------------------- */
            // Hide Buy button and show shipping methods
			$('.buy-now').on('click', function(e) {
				e.preventDefault();
                $(this).siblings('.shipping-method-wrapper').removeClass('hide');
                $(this).addClass('hide');
            });
            // Domestic Shipping
			$('.buy-domestic-shipping').on('click', function(e) {
				e.preventDefault();
				var form = $(this).siblings('form')[0];
				$(form).attr('target', '_blank');
                form.submit();
                $('.buy-now').removeClass('hide');
                $('.shipping-method-wrapper').addClass('hide');
            });
            // International Shipping
			$('.buy-international-shipping').on('click', function(e) {
				e.preventDefault();
				var form = $(this).siblings('form')[1];
				$(form).attr('target', '_blank');
                form.submit();
                $('.buy-now').removeClass('hide');
                $('.shipping-method-wrapper').addClass('hide');
			});
			// Close Button
			$('.close-btn').on('click', function(e) {
				e.preventDefault();
                $(this).parent('.shipping-method-wrapper').siblings('.buy-now').removeClass('hide');
				$(this).parent('.shipping-method-wrapper').addClass('hide');
			});
		/* ----------------------------------------- Buy Button	 */
		
	  

		/*  Default Scripts */
    /* ----------------------------------------- */
    // form control
		$("#search-box input, .wpcf7-form input, .wpcf7-form textarea").focus(function() {
  		$(this).parent().siblings('label').addClass('has-value');
  		$(this).addClass('val');
  	}).blur(function() {
  		var text_val = $(this).val();
  		if(text_val === "") {
  			$(this).parent().siblings('label').removeClass('has-value');
  			$(this).removeClass('val');
  		}
  	});

  	$('.wpcf7-form .date').blur(function() {
	    if( $(this).val().length === 0 ) {
	      $(this).addClass('not-value');
	    }
    });

    // select wrap
		$('select').not('.multiple').wrap('<div class="select-box"></div>');
		
		// fancybox fix with slick
		$().fancybox({
			selector : '.slick-artworks .slick-slide:not(.slick-cloned) figure a',
			backFocus : false,
			afterShow : function( instance, current ) {
				current.opts.$orig.closest(".slick-initialized").slick('slickGoTo', parseInt(current.index), true);
			}
		});

		// smoth scroll
		$('a.scroll, li.scroll a').click(function() {
		  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		    var target = $(this.hash); target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		    if (target.length) { $('html,body').animate({ scrollTop: target.offset().top }, 1000); return false; }
		  }
		});
		/* -----------------------------------------  Default Scripts */
	});
})(jQuery, this);