<div class="container">
	<section class="p p__no-results not-found">
		<header class="p__no-results--header">
			<h1 class="page-title"><?php _e( 'Sorry, there is no content in this section.', 'bfriend' ); ?></h1>
		</header>	
	</section>
</div>