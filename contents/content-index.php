<?php _partial('_pagetitle-tax');  ?>
<div class="row">
  <?php
    if ( have_posts() ) {
      while ( have_posts() ) {
        the_post();
        echo '<div class="col-md-6 col-lg-12 d-flex">';
          _loop('loop-index');
        echo '</div>';
      }
	    echo '<div class="col-md-6 col-lg-12 d-flex ajax--wrapper">';
        global $wp_query;
        $posts_per_page = $wp_query->post_count;
        echo do_shortcode( '[ajax_load_more offset="'. $posts_per_page .'"]');
      echo '</div>';
      wp_reset_postdata();
    } else {
      _content('content-none');
    }
  ?>
</div>