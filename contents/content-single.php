<div class="container">
	<div class="row">

		<div class="col-md-12">
      <?php 
        while ( have_posts() ) :
          the_post();
      ?>
        <article <?php post_class( 'p p__single' ); ?> >
        
					<header class="p__single--header texte-center d-flex flex-column align-items-center">
            <?php 
              the_title( '<h1 class="font-bold mb-3 mb-lg-5">', '</h1>' ); 

              if ( function_exists('yoast_breadcrumb') ) :
                yoast_breadcrumb(' <p id="breadcrumbs" class="d-flex align-self-center mb-classic">','</p> ');
              endif;
            ?>

            <figure class="mb-classic"><img src="<?php echo get_field('2nd_feat_img')['url']; ?>" alt="<?php the_title(); ?>" class="img-fluid  d-block mx-auto"></figure>
            <span class="tag d-none align-items-end justify-content-start"><?php the_post_thumbnail( 'full', ['class' => 'img-fluid'] ); ?></span>
						
					</header>

					<div class="p__single--content row mb-classic">
            <div class="col-md-9 col-lg-8 col-xl-6 mx-auto">
              <time class="mb-4 text-center d-block"><?php the_date(); ?></time>
              <?php the_content(); ?>
            </div>
					</div>
				</article>
			<?php endwhile; ?>
		</div>
	</div>
</div>