<article <?php post_class( 'p p__page' ); ?>>
  
  <header class="page-title sr-only">
    <?php post_type_archive_title('<h1>', '</h1>'); ?>
  </header>
    
  <div class="p p__content">
    <?php $artists = get_artists(); ?>
    <?php $add_artists = get_additional_artists(); ?>
    
    <div class="row">

      <div class="col-md-6 col-md-4 col-lg-5">
        <?php if (count($artists) > 0): ?>
          <h2 class="h3 section-title">Artists</h2>
          <ul class="artists-list">
            <?php 
              foreach ($artists as $artist ) {
                $thumb = has_post_thumbnail($artist->ID) ? get_the_post_thumbnail_url( $artist->ID, 'large') : '';
                $caption = has_post_thumbnail($artist->ID) ? get_the_post_thumbnail_caption( $artist->ID ) : '';
                $href = get_permalink($artist->ID);
                echo '<li><a data-caption="'.$caption.'" data-thumb="'.$thumb.'" class="text-uppercase" href="'.$href.'">'.$artist->post_title.'</a></li>';
              }
            ?>
          </ul>
          <br /> <br />
        <?php endif; ?>

        <?php if (count($add_artists) > 0): ?>
          <h2 class="h3 section-title">Additional Artists Exhibited</h2>
          <ul class="artists-list">
            <?php 
              foreach ($add_artists as $artist ) {
                echo '<li><a class="text-uppercase" href="'.get_permalink($artist->ID).'">'.$artist->post_title.'</a></li>';
              }
            ?>
          </ul>
        <?php endif; ?>
      </div>

      <div class="col-md-6 col-md-8 col-lg-7">
        <div class="artists-thumb d-none d-md-block sticky"></div>
      </div>

    </div>


  </div>

</article>