<?php while ( have_posts() ) : the_post(); ?>
	<?php // _partial('_nav-local-exhibitions') ?>	
	<article <?php post_class( 'p p__single' ); ?> >		
		<header class="text-center pt-4">
			<h1 class="text-uppercase">
					<?php
						$exhibition_name = get_field('exhibition_name', $post_id); 
						$artists_name = get_field('artists_name' );

						if ($exhibition_name) {
							echo '<strong>'. get_the_title() .'</strong> <br />';
							echo '<span class="d-inline-block metric-100 h2">'. $exhibition_name .'</span>';
						} else {
							echo '<strong>'. get_the_title() .'</strong> <br />';
							echo '<span class="d-inline-block metric-100 h2">'. $artists_name .'</span>';
						}
					?>
			</h1>
			<?php 
				echo '<p class="text-uppercase mb-0">' . get_field('exhibition_date', $post_id) .'</p>';
				print_opening_reception($post_id);  				
			?>
			<figure>
				<a href="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ) ?>" class="d-inline-block" data-fancybox>
					<?php the_post_thumbnail( 'full', ['class' => 'img-fluid mt-4'] ); ?>
				</a>
			</figure>
		</header>
		<?php if (get_field('press_release')) : ?>
			<div class="row flex justify-content-center">
				<div class="col-md-8">
					<?php the_field('press_release'); ?>
				</div>
			</div>
		<?php endif; ?>
		
		<?php
			$galleries = get_galleries_by_exhibition(get_the_ID());
			$press = get_field('p_expo');
			$artists = get_field('expo_artist');
			$tour_3d = get_field('3d_tour');
			$pubs = get_publications_by(get_the_ID());
			$class = $press && $artists ? 'col-lg-6' : 'col-12';			

			echo '<div id="artists" class="row">';				
				if ($artists && count($artists) > 0 && count($artists) <= 2) {
					foreach ($artists as $artist ) {
						echo '<div class="col-lg-6">';
							print_artist_preview_block($artist);
						echo '</div>';
					}
				} else if ($artists && count($artists) > 2) { 

					$artistNames = [];
					foreach ($artists as $artist) :
						$artistNames[] = '<a href="'. get_permalink($artist->ID).'" class="" title="Click to read about the artist '. $artist->post_title. '">'. $artist->post_title. '</a>';
					endforeach;
					$artistNamesString = implode(', ', $artistNames);
		?>
				<div class="col-12">
					<div class="block block-artist-preview">
						<div class="block-header">
							<h2>About the Artists</h2>
						</div>
						<div class="block-body">
							<p> Read more about each artist: <?php echo $artistNamesString; ?></p>							
						</div>
					</div> 
				</div> 
		<?php 
				}
				
				if ($press) :
					echo '<div id="press" class="'. $class .'">';
						print_block_press($press);
					echo '</div>';
				endif;

			echo '</div>';
			
			if ($tour_3d) {
				echo '<div id="tour" class="d-flex flex-column">';
					print_block_tour($tour_3d);					
				echo '</div>';				
			}
			echo '<div id="images" class="d-flex flex-column">';
			print_gallery_block($galleries);
			echo '</div>';

			print_video_block($galleries);
			
			print_block_publications($pubs);

			print_upcoming_exhibitions();

		?>

	</article>
<?php endwhile; ?>