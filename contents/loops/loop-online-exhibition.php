<article <?php post_class( 'block__post media flex-column flex-lg-row mb-5 align-items-stretch w-100' ); ?>>
  <?php if (has_post_thumbnail()) : ?>
    <figure class="block__post--header d-flex align-items-center justify-content-center mb-3 mb-lg-0">
      <a href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1" class="d-block thumb">
        <?php the_post_thumbnail( 'full', ['class' => 'img-fluid d-block mx-auto'] ); ?>
      </a>
    </figure>
  <?php endif; ?>

  <div class="media-body block__post--content d-flex flex-column justify-content-between align-items-start">
    <?php
      $is_current_exhibition = get_field('is_current_exhibition');
      if ($is_current_exhibition) {
        echo '<span class="category">Current</span>';
      }
      $subtitle = get_field('subtitle' );
      echo '<a href="'. get_permalink() . '" rel="bookmark" class="">';
        echo '<h2 class="s-title text-uppercase h1 font-bold">';
          echo get_the_title() . '<span class="artists--subtitle mt-0">'.$subtitle.'</span>';
        echo '</h2>';
      echo '</a>';
      echo '<p class="text-uppercase mb-0">' . get_field('exhibition_date') .'</p>';
    ?>
    <ul class="list-inline mb-0">
      <li class="list-inline-item mr-3">
        <a href="<?php the_permalink(); ?>" class="pink font-medium font-24" title="Read more about <?php the_title(); ?>" aria-hidden="true" tabindex="-1">View Exhibition</a>
      </li>    
    </ul>
  </div>
</article>