<?php 
  $post_class = is_archive('publications') ? 
  'block__post__publication media flex-column mb-5 align-items-stretch' :
  'block__post media flex-column flex-lg-row mb-5 align-items-stretch';
?>

<article <?php post_class( $post_class ); ?>>
  <figure class="block__post--header d-flex align-items-center justify-content-center mb-3 mb-lg-0">
    <a href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1" class="d-block thumb">
      <?php the_post_thumbnail( 'full', ['class' => 'img-fluid d-block mx-auto'] ); ?>
    </a>
  </figure>

  <div class="media-body block__post--content d-flex flex-column justify-content-between align-items-start">
    <?php the_publication_title() ?>
    <p><?php echo content(65); ?></p>
    <div class="price">
      <?php print_buy_now_button(); ?>
    </div>
  </div>
</article>