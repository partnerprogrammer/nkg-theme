<article <?php post_class( 'block__post media flex-column flex-lg-row mb-3 align-items-stretch w-100' ); ?>>
  <?php if (has_post_thumbnail()) : ?>
  <figure class="block__post--header d-flex align-items-center justify-content-center mb-3 mb-lg-0">
    <a href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1" class="d-block thumb">
      <img src="<?php echo get_field('2nd_feat_img')['sizes']['news-home']; ?>" alt="<?php the_title(); ?>" class="img-fluid  d-block mx-auto">
      <span class="tag d-flex align-items-end justify-content-start"><?php the_post_thumbnail( 'full', ['class' => 'img-fluid'] ); ?></span>
    </a>
  </figure>
  <?php endif; ?>

  <div class="media-body block__post--content d-flex flex-column justify-content-between align-items-start">
    <?php 
      $cat = get_the_category();  
      if ($cat[0]->term_id != 26) {
        echo '<a href="'.get_category_link($cat[0]->term_id).'" class="category">'.$cat[0]->cat_name.'</a>';        
      }
    ?>
    <?php 
      $title_size = is_front_page() ? 'h2' : 'h1';
      the_title( sprintf( '<h2 class="s-title"><a href="%s" rel="bookmark" class="'.$title_size.' font-bold">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    <time><?php the_date(); ?></time>
    <p><?php echo content(65); ?></p>
    <a href="<?php the_permalink(); ?>" class="pink font-medium font-24" title="Read more: <?php the_title(); ?>" aria-hidden="true" tabindex="-1">Read more</a>
  </div>
</article>