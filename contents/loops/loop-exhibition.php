<article <?php post_class( 'block__post media flex-column flex-lg-row mb-5 align-items-stretch w-100' ); ?>>
  <?php if (has_post_thumbnail()) : ?>
    <figure class="block__post--header d-flex align-items-center justify-content-center mb-3 mb-lg-0">
      <a href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1" class="d-block thumb">
        <?php the_post_thumbnail( 'full', ['class' => 'img-fluid d-block mx-auto'] ); ?>
      </a>
    </figure>
  <?php endif; ?>

  <div class="media-body block__post--content d-flex flex-column justify-content-between align-items-start">
    <?php print_exhibition_header(); ?>    
    <p><?php echo content(65, get_field('press_release')) ?></p>
    
    <ul class="list-inline mb-0">
      <li class="list-inline-item mr-3">
        <a href="<?php the_permalink(); ?>" class="pink font-medium font-24" title="Read more about <?php the_title(); ?>" aria-hidden="true" tabindex="-1">View Exhibition</a>
      </li>    
    </ul>
  </div>
</article>