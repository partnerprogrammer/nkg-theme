<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article <?php post_class( 'p p__page' ); ?>>
		
		<header>
			<?php the_title( '<h1 class="s-title">', '</h1>' ); ?>
    </header>
    
		<div class="p p__content">
			<?php the_content(); ?>
		</div>

	</article>
<?php endwhile; ?>