
<article <?php post_class( 'p p__single' ); ?> >		
	<header class="text-center pt-4">
		<h1 class="text-uppercase">
			<?php					
				$exhibition_name = get_field('exhibition_name'); 
				$subtitle = get_field('subtitle' );						
				echo '<strong>'. get_the_title() .'</strong>';
				if ($subtitle) {
					echo '<br /><span class="d-inline-block metric-100 h2">'. $subtitle .'</span>';					
				}
			?>
		</h1>
		<?php
			if (get_field('exhibition_date')) {
				echo '<p class="text-uppercase mb-0">' . get_field('exhibition_date') .'</p>';					
			}
			if (has_post_thumbnail(get_the_ID())) : ?>	
			<figure>
				<a href="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ) ?>" class="d-inline-block" data-fancybox>
					<?php the_post_thumbnail( 'full', ['class' => 'img-fluid mt-4'] ); ?>
				</a>
			</figure>
		<?php endif; ?>
	</header>
	
	<?php
		$flexibe_content = get_field('flexibe_content');
		if ($flexibe_content) {
			echo '<div class="flexible-content-wrapper">';
				foreach ($flexibe_content as $content) {
					echo '<div class="flexible-content '.$content['acf_fc_layout'].'">';
						switch ($content['acf_fc_layout']) {
							case 'gallery_2_images_by_row':
								print_block_gallery_section_grid_2($content['images'], '', get_the_ID());
								break;
							
								case 'text_content':
									echo '<div class="row justify-content-center">';
										echo '<div class="col-md-8">';
											echo $content['text_content'];
										echo '</div>';
									echo '</div>';
								break;
							
							default:
								
								break;
						}
					echo '</div>';
				} // foreach
			echo '</div>';
		}
	?>

</article>