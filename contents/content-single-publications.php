<div class="container">
	<div class="row">

		<div class="col-md-12">
      <?php 
        while ( have_posts() ) :
          the_post();
      ?>
        <article <?php post_class( 'p p__single' ); ?> >
        
					<header class="p__single--header texte-center d-flex flex-column align-items-center">            
            <?php 
              if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs();
                            
              the_publication_title();

              if ( function_exists('yoast_breadcrumb') ) :
                yoast_breadcrumb(' <p id="breadcrumbs" class="d-flex align-self-center mb-classic">','</p> ');
              endif;
            ?>
            
            <?php if (get_field('pdf_preview')) { ?>            
              <a href="<?php echo get_field('pdf_preview') ?>" target="_blank" rel="nofollow" class="publication-preview d-block w-100">
                <figure class="mb-classic">
                  <?php the_post_thumbnail( 'full', ['class' => 'img-fluid d-block mx-auto'] ); ?>
                  <span class="btn btn-preview">Preview</span>
                </figure>
              </a>
            <?php } else { ?>
              <figure class="mb-classic">
                <?php the_post_thumbnail( 'full', ['class' => 'img-fluid d-block mx-auto'] ); ?>
              </figure>
            <?php } ?>

					</header>

					<div class="p__single--content row mb-classic">
            <div class="col-md-9 col-lg-8 col-xl-6 mx-auto">
              <?php 
                the_content();

                $related = get_field('related_artists_exhibitions');

                if ( $related ) :
                  $names = [];
                  foreach ($related as $r) {
                    $names[] = '<a href="'.get_permalink($r->ID).'" title="Read more about '.$r->post_title.'">'.$r->post_title.'</a>';
                  }
                  echo '<p>Related to: '. implode(", ",$names) .'</p>';
                endif;

                print_buy_now_button();
              ?>
            </div>
					</div>
				</article>
			<?php endwhile; ?>
		</div>
	</div>
</div>