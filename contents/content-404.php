<article <?php post_class('p p__404'); ?> >
  <h1><?php _e( 'Ops, page not found :(', 'pp' ); ?></h1>
  <p><?php _e( 'Sorry, the page you are looking for no longer exists.', 'bfriend' ); ?></p>
  <?php // get_search_form(); ?>
</article>

<!-- <script type="text/javascript">
	document.getElementById('s') && document.getElementById('s').focus();
</script> -->