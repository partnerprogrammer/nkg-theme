<?php while ( have_posts() ) : the_post();  global $post; ?>
	
	<article <?php post_class( 'p p__single' ); ?> >

		<?php			
			$galleries = get_galleries_by_artist(get_the_ID());			

			if (has_post_thumbnail()) {
				echo '<div class="row">';
					echo '<div class="col-lg-5 order-md-2">';
						echo '<figure>';
							the_post_thumbnail( 'full', ['class' => 'img-fluid mb-4'] );
						echo '</figure>';
					echo '</div>';
					echo '<div class="col-lg-7 order-md-1">';
			}

			the_title( '<h1 class="text-uppercase">', '</h1>' );

			$artworks = get_field('artworks');
			$cv = get_field('bio_pdf');

			if ($artworks || $cv) :		
		?>
			<ul class="artist-links list-inline">
				<?php 
					if ($artworks) {
						echo '<a href="#gallery-'.$post->post_name.'" class="js-smooth pink mr-3" title="Click to go to selected works">Selected Artworks</a>';
					}
					if ($cv) {						
						echo '<a href="'.$cv.'" class="pink" rel="nofollow" target="blank" title="Curriculum Vitae - '.get_the_title().'">Curriculum Vitae</a>';
					}
				?>
			</ul>
		<?php endif ?>
		
		<div class="p__single--content">
			<?php the_content(); ?>
		</div>

		<?php if (has_post_thumbnail()) {
				echo '</div>'; // .col-md-8
			echo '</div>'; // .row
		} ?>

		<?php 		

			if ($artworks) {
				// print_block_gallery_section($artworks, $post->post_title . ' Selected Artworks ', 'gallery-'.$post->post_name);
				// print_block_gallery_section_full($artworks, $post->post_title . ' Selected Artworks ', 'gallery-'.$post->post_name);
				// print_block_gallery_section_masonry($artworks, $post->post_title . ' Selected Artworks ', 'gallery-'.$post->post_name);
				print_block_gallery_section_grid($artworks, $post->post_title . ' Selected Artworks ', 'gallery-'.$post->post_name);
				// print_block_gallery_section_grid_4($artworks, $post->post_title . ' Selected Artworks ', 'gallery-'.$post->post_name);
			}

			if (count($galleries) > 0) {
				foreach ($galleries as $post) {
					$images = get_field('images', $post->ID);
					if ($images) {
						// print_block_gallery_section_masonry($images, $post->post_title, 'gallery-' . $post->ID);
						print_block_gallery_section_grid($images, $post->post_title, 'gallery-' . $post->ID);
					}				
				}
				wp_reset_postdata();
			}

			$exhibitions = get_exhibitions_by_artist(get_the_ID());
			if ($exhibitions): ?>
			<div id="exhibitions" class="block block-artworks">
				<div class="block-header">
					<h2><?php the_title() ?> Exhibitions</h2>
					<div class="block-header-navigation"></div>
				</div>
				<div class="block-body">
					<div class="row">
						<?php 
							foreach ($exhibitions as $exhibition): 
								$post_id = $exhibition->ID;
								$title = $exhibition->post_title;								
								$name = get_field('exhibition_name', $post_id);
						?>
							<div class="col-md-4">
								<article class="mb-4">
									<h2 class="h6 text-uppercase bold mb-0">
										<?php 
											echo $title;
											if ($name) {
												echo '<br/>' . $name;
											}	
										?>
									</h2>
									<p class="text-uppercase mb-0"><?php the_field('exhibition_date', $post_id); ?></p>
									<?php print_opening_reception($post_id); ?>
									<a class="pink" href="<?php echo get_permalink($post_id) ?>" title="<?php echo $title . ' - ' . $name; ?>">View Exhibition</a>
								</article>
							</div>
						<?php endforeach; ?>
					</div>								
				</div>
			</div>
		<?php endif; ?>

		<?php 			
			$press = get_field('press_art');
			if ($press) :
				print_block_press($press);
			endif; 
		?>

		<?php 
			$pubs = get_publications_by(get_the_ID());
			if ($pubs) :
		?>
			<div id="publications" class="block block-publications">
				<div class="block-header">
					<h2>Publications</h2>
					<div class="block-header-navigation"></div>
				</div>
				<div class="block-body">					
					<div class="slick-publications slick-arrowsaside">
						<?php 
							global $post;
							foreach ($pubs as $post ) :
								setup_postdata($post);
						?>
							<div class="">
								<article class="row">
									<div class="col-sm-6">
										<div class="content-wrapper">
											<?php the_publication_title() ?>

											<div class="img-wrapper d-md-none">
												<?php the_post_thumbnail( 'publication-thumb', ['class' => 'img-fluid mb-4'] ); ?>
											</div>

											<div class="content">
												<?php the_excerpt() ?>	
												<?php print_buy_now_button(); ?>
											</div>

										</div>
									</div>
									<div class="col-sm-6">
										<div class="img-wrapper d-none d-md-block">
											<?php the_post_thumbnail( 'publication-thumb', ['class' => 'img-fluid mx-auto'] ); ?>
										</div>
									</div>
								</article>
							</div>
						<?php wp_reset_postdata(); endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php print_video_block($galleries); ?>

	</article>
<?php endwhile; ?>