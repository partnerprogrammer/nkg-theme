<?php
global $wp_query;

$args = [ 'post_type' => 'online-exhibition', 'meta_key' => 'is_current_exhibition', 'meta_value' => 1 ];
$currentOnlineExhibitionsQuery = new WP_Query($args); 
if (isset($_GET['redirect_to_first']) && $_GET['redirect_to_first'] && $currentOnlineExhibitionsQuery->found_posts > 0) {
  wp_safe_redirect(get_permalink($currentOnlineExhibitionsQuery->posts[0]->ID), 302);
}

wp_reset_postdata();

  get_header();

    _partial('_wrap-start');
      
      _partial('_nav-tax-year');
      _partial('_pagetitle-tax');

      echo '<div class="row">';
        if ( have_posts() ) {
          while ( have_posts() ) {
            the_post();
            echo '<div class="col-12 d-flex">';
              _loop('loop-online-exhibition');
            echo '</div>';
          }
          if (function_exists('wp_pagenavi')) { wp_pagenavi(); };

          // show_next_term_link(
          //   get_terms(['taxonomy' => 'exhibition_year', 'hide_empty' => false ]),
          //   'exhibitions'
          // );

          wp_reset_postdata();
        } else {
          _content('content-none');
        }
      echo '</div>';

    _partial('_wrap-end');
      
  get_footer();