<?php _partial('_pagetitle-tax'); ?>
<div class="row">
  <?php

    $args = ['post_type' => 'publications', 'posts_per_page' => -1 ];
    $query = new WP_Query($args); 
    if ($query->have_posts()):
      while ($query->have_posts()): $query->the_post();
        echo '<div class="col-md-6 col-xl-4 col-fi-3 d-flex">';
          _loop('loop-publication');
        echo '</div>';    
      endwhile;
    endif; wp_reset_postdata();
  ?>
</div>