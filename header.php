<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <style>
      .bsnav-mobile {
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        position: fixed;
        z-index: 49;
        pointer-events: none;
      }
      .bsnav-mobile .bsnav-mobile-overlay {
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        position: fixed;
        visibility: hidden;
        pointer-events: none;
      }
      .bsnav-mobile .navbar {
        background: #fff;
        width: 230px;
        padding: 70px 15px 15px;
        text-transform: uppercase;
        border-left: 8px solid #000;
        flex-flow: column;
        top: 0;
        bottom: 0;
        right: 0;
        position: absolute;
        transition: .4s ease-in-out;
        transform: translate3d(300px,0,0);
        overflow: auto;
      }
    </style>
		<!-- <link rel="preload stylesheet" as="style" type="text/css" media="all"  onload="this.onload=null;this.rel='stylesheet'" href="<?php bloginfo( 'template_url' ); ?>/assets/css/main.min.css" />
		<link rel="preload stylesheet" as="style" type="text/css" media="all"  onload="this.onload=null;this.rel='stylesheet'" href="<?php bloginfo( 'stylesheet_url' ); ?>" /> -->
		<!-- <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/assets/css/main.min.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" /> -->
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link href="<?php images_url('favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Nathalie Karg Gallery presents an inclusive program of contemporary art that is as engaging and thought-provoking as it is irreverent and conceptually driven. Our roster showcases diverse aesthetic interests, reflecting our mission to think beyond the medium."/>
    
    <?php wp_head(); ?>
	</head>
  <body <?php body_class(); ?>>
    <!-- <div class="preloader"><div class="preloader__loader"><?php //for ($i = 1; $i <= 2; $i++) { echo '<div></div>'; } ?></div></div> -->

    <header class="main-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <nav id="navbar" class="navbar navbar-expand-lg bsnav">
              <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <h1 class="text-uppercase mb-0"><?php bloginfo('name'); ?></h1>
              </a>
              <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon">Menu</span></button>
              <?php
                wp_nav_menu([
                  'theme_location'  => 'global',
                  'container'       => 'div',
                  'container_class' => 'collapse navbar-collapse justify-content-md-end',
                  'menu_id'         => false,
                  'menu_class'      => 'navbar-nav navbar-mobile',
                  'depth'           => 2,
                  // 'items_wrap'      => nav_wrap(),
                  'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                  'walker'          => new wp_bootstrap_navwalker()
                ]);
              ?>
            </nav>
          </div>
        </div>
      </div>
    </header>
        
    <div class="bsnav-mobile">
      <div class="bsnav-mobile-overlay"></div>
      <div class="navbar"></div>
    </div>