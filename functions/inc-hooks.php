<?php 

/**
 * Dynamic Select List for Contact Form 7
 * @usage [select name cpt:{$slug_cpt} ...]
 * 
 * @param Array $tag
 * 
 * @return Array $tag
 */
function dynamic_select_list_cpt( $tag ) {
    
    // Only run on select lists
    if( 'select' !== $tag['basetype'] ) {
        return $tag;
    } else if ( empty( $tag['options'] ) ) {
        return $tag;
    }

    $queryArgs = array(
        'posts_per_page' => -1
    );

    if (substr($tag['options'][0], 0, 9) != 'post_type') return $tag;
    // Loop thorugh options to look for our custom options
    foreach( $tag['options'] as $option ) {

        $matches = explode( ':', $option );

        if( ! empty( $matches ) ) {
            switch( $matches[0] ) {

                case 'post_type':
                    $queryArgs['post_type'] = $matches[1];
                    break;

            }
        }

    }

    // Ensure we have a term arguments to work with
    if( empty( $queryArgs ) ) {
        return $tag;
    } 

    $posts = get_posts( $queryArgs );

    // Add terms to values
    if( ! empty( $posts ) ) {
        
        foreach( $posts as $post ) {
            // echo '<pre>'.print_r($tag,1). '</pre>';
            // die();
            $tag['values'][$post->ID] = $post->post_title;

        }

    }
    wp_reset_postdata();

    return $tag;

}
add_filter( 'wpcf7_form_tag', 'dynamic_select_list_cpt', 10 );

/* ----------------------------------------- Contact Form 7 */


add_filter('post_type_link', 'cj_update_permalink_structure', 10, 2);
function cj_update_permalink_structure( $post_link, $post )
{
    if ( false !== strpos( $post_link, '%exhibition_year%' ) ) {
        $taxonomy_terms = get_the_terms( $post->ID, 'exhibition_year' );				
        if ($taxonomy_terms) {
            foreach ( $taxonomy_terms as $term ) { 
                if ( ! $term->parent ) {
                    $post_link = str_replace( '%exhibition_year%', $term->slug, $post_link );
                }
            }
        } 
    }
		
    return $post_link;
}


function remove_pagination_on_archive_publications($query) {
    if ( !is_admin() && $query->is_main_query() && is_post_type_archive('publications') ) {        
        $query->set('posts_per_page', '-1');
        // $query->set('orderby', 'meta_value');
        // $query->set('meta_key', 'custom_order');
        // $query->set('order', 'ASC');
    }
}
// add_action('pre_get_posts','remove_pagination_on_archive_publications');



/**
 * Change the Rank Math Metabox Priority
 *
 * @param array $priority Metabox Priority.
 */
add_filter( 'rank_math/metabox/priority', function( $priority ) {
    return 'low';
});
   
/**
 * Add 
 */
add_filter( 'body_class', function( $classes ) {
    if (is_single() && get_field('add_dark_theme')) {
        return array_merge( $classes, array( 'dark-theme' ) );        
    } else {
        return $classes;
    }
} );