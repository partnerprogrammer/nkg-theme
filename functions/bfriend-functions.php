<?php

// wrap item on nav
function nav_wrap() {
  $wrap  = '<ul id="%1s" class="%2s">';
    $wrap .= '%3s';
    $wrap .= '<li class="nav-item nav-search">'. get_search_form([ 'echo' => false ]) .'</li>';
  $wrap .= '</ul>';
  return $wrap;
}