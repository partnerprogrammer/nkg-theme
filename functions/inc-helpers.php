<?php 

function get_artists() {
  return get_field('artists', 'option');
}

function get_additional_artists() { 
  return get_field('additional_artists', 'option');
}

function get_exhibitions_by_artist($artist_id) {
  $args = [
    'post_type' => 'exhibitions',
		'meta_query' => array(
			array(
				'key' => 'expo_artist', // name of custom field
				'value' => '"' . $artist_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
    )
  ];
  $query = new WP_Query($args); 
  return $query->posts;
}

function get_publications_by($post_id) {
  $args = [
    'post_type' => 'publications',
		'meta_query' => array(
			array(
				'key' => 'related_artists_exhibitions', // name of custom field
				'value' => '"' . $post_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
    )
  ];
  $query = new WP_Query($args); 
  return $query->posts;
}

function get_galleries_by_artist($post_id) {
  $args = [
    'post_type' => 'gallery-images',
		'meta_query' => array(
			array(
				'key' => 'rel_artists', // name of custom field
				'value' => '"' . $post_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
		)
  ];
  $query = new WP_Query($args);
  return $query->posts;
}

function get_galleries_by_exhibition($post_id) {
  $args = [
    'post_type' => 'gallery-images',
		'meta_query' => array(
			array(
				'key' => 'rel_exhibition', // name of custom field
				'value' => '"' . $post_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
		)
  ];
  $query = new WP_Query($args);
  return $query->posts;
}




function print_block_gallery_section($artworks, $title  = 'Artworks', $id = false) {
  $id = $id ? $id : 'gallery-' . sanitize_title($title);
?>
  <div id="<?php echo $id; ?>" class="block block-artworks">
    <div class="block-header">
      <h2><?php echo $title; ?></h2>
      <div class="block-header-navigation"></div>
    </div>
    <div class="block-body">
      <div class="slick-artworks">					
        <?php foreach ($artworks as $artwork): ?>
          <div>
            <figure>
              <div class="img-wrapper">
                <a href="<?php echo $artwork['sizes']['2048x2048']; ?>" data-fancybox="fancy-<?php echo $id ?>" title="<?php echo $artwork['alt']; ?>">
                  <img class="img-fluid" src="<?php echo $artwork['sizes']['large']; ?>" alt="<?php echo $artwork['alt']; ?>" />
                </a>
              </div>
              <?php if ($artwork['caption']): ?>
              <figcaption>
                <p><?php echo $artwork['caption']; ?></p>
              </figcaption>
              <?php endif; ?>
            </figure>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  
<?php
}
function print_block_gallery_section_full($artworks, $title  = 'Artworks', $id = false) {
  $id = $id ? $id : 'gallery-' . sanitize_title($title);
?>
  <div id="<?php echo $id; ?>" class="block block-artworks">
    <div class="block-header">
      <h2><span class="text-primary">Source Images Thumbnails</span> <?php echo $title; ?></h2>
      <div class="block-header-navigation"></div>
    </div>
    <div class="block-body">
      <div class="slick-artworks">					
        <?php foreach ($artworks as $artwork): ?>        
          <div>
            <figure>
              <div class="img-wrapper">
                <a href="<?php echo $artwork['url']; ?>" data-fancybox="fancy-<?php echo $id ?>" title="<?php echo $artwork['alt']; ?>">
                  <img class="img-fluid" src="<?php echo $artwork['url']; ?>" alt="<?php echo $artwork['alt']; ?>" />
                </a>
              </div>
              <?php if ($artwork['caption']): ?>
              <figcaption>
                <p><?php echo $artwork['caption']; ?></p>
              </figcaption>
              <?php endif; ?>
            </figure>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  
<?php
}
function print_block_gallery_section_masonry($artworks, $title  = 'Artworks', $id = false) {
  $id = $id ? $id : 'gallery-' . sanitize_title($title);
?>
  <div id="<?php echo $id; ?>" class="block block-artworks block-artworks-masonry">
    <div class="block-header">
      <h2><span class="text-primary">Masonry</span> <?php echo $title; ?></h2>
      <div class="block-header-navigation"></div>
    </div>
    <div class="block-body">
      <!-- <div class="slick-artworks">				 -->
        <?php foreach ($artworks as $artwork): ?>
          <div>
            <figure>
              <div class="img-wrapper">
                <a href="<?php echo $artwork['url']; ?>" class="d-block" data-fancybox="fancy-<?php echo $id ?>" title="<?php echo $artwork['alt']; ?>">
                  <img class="img-fluid" src="<?php echo $artwork['url']; ?>" alt="<?php echo $artwork['alt']; ?>" />
                </a>
              </div>
              <?php if ($artwork['caption']): ?>
              <figcaption>
                <p><?php echo $artwork['caption']; ?></p>
              </figcaption>
              <?php endif; ?>
            </figure>
          </div>
        <?php endforeach; ?>
      <!-- </div> -->
    </div>
  </div>
  
<?php
}

function print_attachment_caption($id, $inquire = false) {
  $fields = get_fields($id);
  // echo '<pre>'.print_r($fields,1). '</pre>';
  if ($fields) {
    echo '<figcaption class="text-center mb-4">';
      foreach ($fields as $key => $value) {
        if ($value) {          
          if ($key === 'price') {
            echo '<p class="mb-0 mt-3 '.$key.'">'.'$ ' . number_format($value) . '</p>';            
          } else {
            echo '<p class="mb-0 '.$key.'">'.$value.'</p>';
          }
        }
      }
      if ($inquire) {
        echo '<a class="btn-inquire mt-3" href="mailto:monica@nathaliekarg.com?subject=Nathalie Karg Gallery - Inquire '. $fields['title_of_piece'].' / '. $fields['artist_name'].'" target="_blank">Inquire</a>';
      }
    echo '</figcaption>';
  } 
}

function print_block_gallery_section_grid($artworks, $title  = 'Artworks', $id = false) {
  $id = $id ? $id : 'gallery-' . sanitize_title($title);
?>
  <div id="<?php echo $id; ?>" class="block block-artworks block-artworks-grid">
    <div class="block-header">
      <h2><?php echo $title; ?></h2>
      <div class="block-header-navigation"></div>
    </div>
    <div class="block-body">      
      <?php foreach ($artworks as $artwork): ?>
          <figure>
            <div class="img-wrapper">
              <a href="<?php echo $artwork['url']; ?>" class="d-block" data-fancybox="fancy-<?php echo $id ?>" title="<?php echo $artwork['alt']; ?>">
                <!-- <img src="<?php echo $artwork['url']; ?>" data-no-lazy="1" alt="<?php echo $artwork['alt']; ?>" /> -->
                <img class="img-fluid" src="<?php echo $artwork['url']; ?>" data-no-lazy="1" alt="<?php echo $artwork['alt']; ?>" />
              </a>
            </div>
            <?php print_attachment_caption($artwork['ID']) ?>
          </figure>
      <?php endforeach; ?>      
    </div>
  </div>
  
<?php
}

function print_block_gallery_section_grid_2($artworks, $title  = 'Artworks', $id = false) {
  $id = $id ? $id : 'gallery-' . sanitize_title($title);
?>
  <div id="<?php echo $id; ?>" class="block block-artworks block-artworks-grid-2">
    <div class="block-body">      
      <?php foreach ($artworks as $artwork): ?>
          <figure>
            <div class="img-wrapper text-center">
              <a href="<?php echo $artwork['url']; ?>" class="d-inline-block" data-fancybox="fancy-<?php echo $id ?>" title="<?php echo $artwork['alt']; ?>">
                <img class="img-fluid" src="<?php echo $artwork['url']; ?>" data-no-lazy="1" alt="<?php echo $artwork['alt']; ?>" />
              </a>
            </div>
            <?php print_attachment_caption($artwork['ID'], $inquire = true ) ?>
          </figure>
      <?php endforeach; ?>      
    </div>
  </div>
  
<?php
}



function print_block_video_section($video, $title  = 'Video') {
  if (!$video) return '';
?>
  <div id="video-<?php echo sanitize_title($title) ?>" class="block block-video">
    <div class="block-header">
      <h2><?php echo $title; ?></h2>
    </div>
    <div class="block-body">
      <div class="embed-container">
        <?php echo $video ?>
      </div>
    </div>
  </div>
  
<?php
}


function show_next_term_link($terms, $title) {
  $current_term = get_queried_object();  
  if ($terms) {
    // echo '<pre>'.print_r($terms,1). '</pre>';    
    // echo '<pre>'.print_r($current_term,1). '</pre>';
    echo '<div class="col-12 text-right">';
    foreach ($terms as $term) {
      if ($next) {
        echo '<a class="pink font-24" title="Check out the '.$term->name.' '.$title.'" href="'.get_term_link($term).'">Check out the '.$term->name.' '.$title.'</a>';
        $next = false;
      }
      if ($term == $current_term) {
        $next = true;
      }      
    }
    echo '</div>';
  }
}

function print_exhibition_year($post_id) {
  $is_current = get_field('is_current_exhibition', $post_id);
  $term = get_the_terms($post_id, 'exhibition_year');
  
  if (!$is_current && is_tax()) return false;

  if ($is_current) {
    echo '<span class="category">Current</span>';
  } elseif ($term && !is_tax()) {
    echo '<a href="'.get_term_link($term[0]).'" class="category">'.$term[0]->name.'</a>';        
  } elseif ($term && $term[0]) {
    echo '<span class="category">'.$term[0]->name.'</span>';
  }  
}

function print_exhibition_header($post_id = false) {
  $post_id = $post_id ? $post_id : get_the_ID();

  print_exhibition_year($post_id);
  
  $exhibition_name = get_field('exhibition_name', $post_id);
  $artists_name = get_field('artists_name', $post_id);
  $subtitle_type = $exhibition_name ? 'exhibition' : 'artists';
  $subtitle = $exhibition_name ? '<br/>'. $exhibition_name : '<br/>' . $artists_name;
  
  $is_single = is_single($post_id) && 'exhibitions' == get_post_type();
  $tag = $is_single ? 'h1' : 'h2';
  
  $is_related = is_single() && !is_single($post_id) ? true : false;
  $cssClass = $is_related ? '' : 'h1';

  
  if (!$is_single) {
    echo '<a href="'. get_permalink($post_id) . '" rel="bookmark" class="">';
  }
    echo '<'.$tag.' class="s-title text-uppercase '.$cssClass.' font-bold">';
      echo get_the_title($post_id) . '<span class="'.$subtitle_type.'--subtitle">'.$subtitle.'</span>';
    echo '</'.$tag.'>';
    
  if (!$is_single) {
    echo '</a>';
  }


  echo '<p class="text-uppercase mb-0">' . get_field('exhibition_date', $post_id) .'</p>';
    print_opening_reception($post_id);  
  echo '<br />';


}


function print_block_press($press) {
?>
  <div id="press" class="block block-press">
    <div class="block-header">
      <h2>Press</h2>
    </div>
    <div class="block-body">
      <ul class="list-inline">
        <?php
          foreach ($press as $p ) {
            $title = isset($p['a_p_title']) ? $p['a_p_title'] : $p['e_p_title'];
            $link = isset($p['a_p_link']) ? $p['a_p_link'] : $p['e_p_link'];
            if ($title) {
              echo '<li><a class="pink d-inline-block" target="_blank" rel="nofollow" href="'.$link.'" title="'.$title.'">'.$title.'</a></li>';
            }
          } 
        ?>
      </ul>
    </div>
  </div>
<?php
}

function print_block_tour($code) {
?>
  <div id="press" class="block block-press">
    <div class="block-header">
      <h2>3D Tour</h2>
    </div>
    <div class="block-body">
      <?php echo $code ?>
    </div>
  </div>
<?php
}




function print_block_publications($pubs) {
  if ($pubs && count($pubs) > 0) :
?>
  <div id="publications" class="d-flex">
    <div class="block block-publications">
      <div class="block-header">
        <h2>Publications</h2>
        <div class="block-header-navigation"></div>
      </div>
      <div class="block-body">					
        <div class="slick-publications">
          <?php 
            global $post;
            foreach ($pubs as $post ) :
              setup_postdata($post);
              
          ?>
            <div class="">
              <article class="row">
                <div class="col-sm-6">
                  <div class="content-wrapper">
                    <h2><?php the_title() ?></h2>

                    <div class="img-wrapper d-md-none">
                      <?php the_post_thumbnail( 'publication-thumb', ['class' => 'img-fluid mb-4'] ); ?>
                    </div>

                    <div class="content">
                      <?php the_excerpt() ?>
                      <?php print_buy_now_button(); ?>
                    </div>
                    
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="img-wrapper d-none d-md-block">
                    <?php the_post_thumbnail( 'publication-thumb', ['class' => 'img-fluid mx-auto'] ); ?>
                  </div>
                </div>
              </article>
            </div>
          <?php wp_reset_postdata(); endforeach; ?>
        </div>
      </div>
    </div>
  </div>
<?php
  endif;
}


function print_block_exhibitions($exhibitions) { ?>
  <div id="exhibitions" class="block block-exhibitions">
    <div class="block-header">
      <h2><?php echo ucwords(strtolower(get_the_title())) ?> Exhibitions</h2>
      <div class="block-header-navigation"></div>
    </div>
    <div class="block-body">
      <div class="row">
        <?php 
          foreach ($exhibitions as $exhibition): 
            print_exhibition_teaser($exhibition);
          endforeach;
        ?>
      </div>								
    </div>
  </div>
<?php
}

function has_video_in_galleries($galleries) {
  $has_video = false;
  
  if (count($galleries) > 0) {
    foreach ($galleries as $post) {
      $videos = get_field('rep_videos', $post->ID, true);
      
      if ($videos && count($videos) > 0) {
        $has_video = true;
        break;
      }					
    }
  }

  return $has_video;
}

function print_video_block($galleries) {
  if (count($galleries) > 0) {
    foreach ($galleries as $post) {
      $videos = get_field('rep_videos', $post->ID, true);
      
      if ($videos && count($videos) > 0) {
        foreach ($videos as $video ) {
          if ($video && isset($video['video']) && $video['video']) {
            print_block_video_section($video['video']);
          }		
        }
      }					
    }
  }  
}

function print_gallery_block($galleries) {
  if (count($galleries) > 0) {
    foreach ($galleries as $post) {
      $artworks = get_field('images', $post->ID);
      if ($artworks) {
        print_block_gallery_section_grid($artworks, $post->post_title, 'gallery-' . $post->ID);
        // print_block_gallery_section_masonry($artworks, $post->post_title, 'gallery-' . $post->ID);
      }				
    }
  }  
}

function print_artist_preview_block($post) { ?>
  <div class="block block-artist-preview">
    <div class="block-header">
      <h2>About <?php echo ucwords(strtolower($post->post_title)) ?></h2>
    </div>
    <div class="block-body">
      <p> <?php echo content(65, $post->post_content) ?> </p>
      <ul class="list-inline mb-0">
        <li class="list-inline-item mr-3">
          <a href="<?php the_permalink($post->ID); ?>" class="pink font-medium" title="Read more about <?php echo $post->post_title ?>" aria-hidden="true" tabindex="-1">View Artist</a>
        </li>    
      </ul>
    </div>
  </div>  
<?php
}

function print_opening_reception($post_id) {
  $is_current = get_field('is_current_exhibition', $post_id);
  if ((has_term(['upcoming'], 'exhibition_year', $post_id) || $is_current) && get_field('opening_reception', $post_id)): ?>
    <p class="text-uppercase mb-0">OPENING RECEPTION: <?php the_field('opening_reception', $post_id); ?></p>      
  <?php endif;
}


function print_upcoming_exhibitions() {
  $upcoming = get_upcoming_exhibitions() ?? array();  
  $related = get_related_exhibitions() ?? array();

  $exhibitions = array_merge($upcoming, $related);

  if ($upcoming && $related) {
    $title = 'Upcoming & related';
  } elseif ($upcoming) {
    $title = 'Upcoming';
  } elseif ($related) {
    $title = 'Related';
  }

  if ($exhibitions) {
?>
  <div id="exhibitions" class="block block-exhibitions">
    <div class="block-header">
      <h2><?php echo $title; ?> exhibitions</h2>
      <div class="block-header-navigation"></div>
    </div>
    <div class="block-body">
      <div class="row">
        <?php 
          foreach ($exhibitions as $exhibition): 
            print_exhibition_teaser($exhibition);
          endforeach; 
        ?>
      </div>								
    </div>
  </div>
<?php
  }
}


function get_upcoming_exhibitions() {
  $current_post = get_the_ID();
  $args = [
    'post_type' => 'exhibitions',
    'posts_per_page' => -1,
    'exhibition_year' => 'upcoming',
    'post__not_in' => array($current_post)
  ];
  $query = new WP_Query($args);
  $return = $query->posts;
  wp_reset_postdata();
  return $return;
}


function get_related_exhibitions() {

  $artists = get_post_meta( get_the_ID(), 'expo_artist', true );    
  $return = array();
  
  if ($artists && count($artists) > 0) {
    foreach ($artists as $artist_id ) {
      $posts = get_related_exhibitions_by_artist($artist_id) ?? array();
      $return = array_merge($return, $posts);
    }
  }
  
  return $return;
}

function get_related_exhibitions_by_artist($artist_id) {
  $current_post = get_the_ID();
  $args = [
    'post_type' => 'exhibitions',
    'posts_per_page' => -1,
    'post__not_in' => array($current_post),
    'meta_query' => array(
			array(
				'key' => 'expo_artist', // name of custom field
				'value' => '"' . $artist_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
    )
  ];
  $query = new WP_Query($args);
  $return = $query->posts;
  wp_reset_postdata();
  return $return;
}


function print_exhibition_teaser($exhibition) {
  $post_id = $exhibition->ID;
  $title = $exhibition->post_title;
  $name = get_field('exhibition_name', $post_id);
?>
<div class="col-md-6">
  <article class="mb-4 exhibition-teaser">
    <?php print_exhibition_header($post_id); ?>    
    <a class="pink" href="<?php echo get_permalink($post_id) ?>" title="<?php echo $title . ' - ' . $name; ?>">View Exhibition</a>
  </article>
</div>
<?php 

}


function print_buy_now_button() {
  $price = get_field('price');
  $soldOut = get_field('sold_out');

  if ( $price && $soldOut[0] == 'yes') :
    echo '<p class="sold-out d-inline-block">Sold Out</p>';
    if ( is_post_type_archive('publications') || get_query_var('post_type') != 'publications') {
      echo '<a class="d-inline-block ml-4 pink" href="'. get_permalink() .'" title="Read more about '.get_the_title().'">Read More</a>';
    }
    echo '<div class="clearfix mb-5"></div>';
  else :
    $obj = get_field_object('price');

    if ( is_single() ) {
      $isSingle = 'on-single';
    } else {
      $isSingle = "";
    }
    
    // echo '<pre>'.print_r(get_field('domestic_shipping'),1). '</pre>';
    
    echo '<p class="btn-price-number">'. $obj['prepend'] . ' ' . $price . ' ' . $obj['append'] . '</p>';
    echo '<a class="pink font-medium font-24 buy-now" href="#" title="Click to buy" rel="nofollow" >Buy Now</a>';

    echo '<div class="shipping-method-wrapper '.$isSingle.' hide">';
      echo do_shortcode( '[wp_paypal shipping="'. domestic_shipping_price() .'" tax="'.$price * 0.08875.'" button="buynow" name="'.get_the_title().'" no_shipping="2" amount="'. $price .'.00"]');
      echo '<a class="pink font-medium font-24 buy-domestic-shipping" href="#" title="Click to buy" rel="nofollow" >Domestic shipping</a>';
      echo do_shortcode( '[wp_paypal shipping="'. international_shipping_price() .'" tax="'.$price * 0.08875.'" button="buynow" name="'.get_the_title().'" no_shipping="2" amount="'. $price .'.00"]');
      echo '<a class="pink font-medium font-24 buy-international-shipping" href="#" title="Click to buy" rel="nofollow" >International Shipping</a>';
      echo '<a class="pink close-btn" href="#" rel="nofollow" title="Close">x</a>';
    echo '</div>';

    // echo '<a class="pink font-medium font-24 buy-now" href="#" title="Click to buy" rel="nofollow" >Buy Now</a>';
    if ( is_post_type_archive('publications') || get_query_var('post_type') != 'publications') {
      echo '<a class="d-inline-block ml-4 pink" href="'. get_permalink() .'" title="Read more about '.get_the_title().'">Read More</a>';
    }
    echo '<div class="clearfix mb-5"></div>';
  endif;
}


function domestic_shipping_price() {
  $domestic_shipping = get_field('domestic_shipping');
  if( $domestic_shipping ):
    $obj = get_field_object('domestic_shipping');
    $domestic_shipping_price = ($domestic_shipping['custom'] ? $domestic_shipping['custom'] : $domestic_shipping['default']);
  endif;

  return $domestic_shipping_price;
}

function international_shipping_price() {
  $international_shipping = get_field('international_shipping');
  if( $international_shipping ):
    $obj = get_field_object('international_shipping');
    $international_shipping_price = ($international_shipping['custom'] ? $international_shipping['custom'] : $international_shipping['default']);
  endif;

  return $international_shipping_price;
}


function the_publication_title() {
  global $post, $wp_query;

  $title = get_field('custom_title') ? get_field('custom_title') : get_the_title();
  $subtitle = get_field('subtitle');
  $is_h1 = is_single() && get_post_type($wp_query->post) == 'publications' ? true : false;
  
  if ($is_h1) {
    echo '<h1 class="s-title font-bold mb-3 mb-lg-5 mt-lg-2">';
  } else {
    echo sprintf( '<h2 class="s-title"><a href="%s" rel="bookmark" class="h2 font-bold">', esc_url( get_permalink() ));
  }
    
    echo $title;
    if ($subtitle) {
      echo '<span class="d-block thin-italic">'.$subtitle.'</span>';
    }
  
  if ($is_h1) {
    echo '</h1>';
  } else {
    echo '</a></h2>';
  }

}


if (!function_exists("pretty_dump")) {
  function pretty_dump($s, $return=false) {
      $x = "<pre>";
      $x .= print_r($s, 1);
      $x .= "</pre>";
      if ($return) return $x;
      else print $x;
  }
}