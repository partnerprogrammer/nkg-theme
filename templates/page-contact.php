<?php
  /* Template name: Page Info */
  get_header();

	if ( have_posts() ) while ( have_posts() ) : the_post(); 
  _partial('_wrap-start');

?>
  <article <?php post_class( 'p p__company' ); ?>>
    <header class="p__company--header">
      <?php the_title('<h1>', '</h1>'); ?>
    </header>
    
    <div class="p__company--content">
      <?php the_content(); ?>
    </div>

    <div class="infos mt-4 mb-5 d-flex flex-column flex-lg-row justify-content-lg-between">
      <?php if (get_field('address', 'option')) : ?>
        <div class="address">
          <p class="font-bold text-uppercase mb-0">Address</p>
          <p><?php the_field('address', 'option') ?></p>
        </div>
      <?php endif; ?>
      
      <?php if (get_field('hours', 'option')) : ?>
        <div class="hours">
          <p class="font-bold text-uppercase mb-0">Hours</p>
          <p><?php the_field('hours', 'option') ?></p>
        </div>
      <?php endif; ?>
      
      <?php if (get_field('telephone', 'option')) : ?>
        <div class="telephone">
          <p class="font-bold text-uppercase mb-0">Telephone</p>
          <p><?php the_field('telephone', 'option') ?></p>
        </div>
      <?php endif; ?>
      
      <?php if (get_field('email', 'option')) : ?>
        <div class="email">
          <p class="font-bold text-uppercase mb-0">Email</p>
          <p><a class="pink" target="_blank" href="mailto:<?php the_field('email', 'option') ?>" rel="nofollow" title="Contact us"><?php the_field('email', 'option') ?></a></p>
        </div>
      <?php endif; ?>
      
      <?php if (get_field('instagram', 'option')) : ?>
        <div class="instagram">
          <p class="font-bold text-uppercase mb-0">Instagram</p>
          <p><a class="pink" target="_blank" href="<?php the_field('instagram', 'option') ?>" rel="nofollow" title="Follow us at instagram">@nathaliekarggallery</a></p>
        </div>
      <?php endif; ?>
    </div>    

    <?php $location = get_field('google_maps'); if( $location ): ?>
        <div class="acf-map" data-zoom="16">
            <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
        </div>
    <?php endif; ?>

    
  </article>
<?php 
  _partial('_wrap-end');
  endwhile; 
  get_footer();
?>