<?php 
  $terms = get_terms(['taxonomy' => 'exhibition_year', 'hide_empty' => true ]);
  $current_term = get_queried_object();
  // echo '<pre>'.print_r($current_term,1). '</pre>';
  // die();
  if ($terms) :
?>
  <div id="nav-tax-year" class="navigation-local mb-5">
        
    <div class="d-lg-none">      
      <select class="js-navigate-onchange">
        <option value="<?php echo get_post_type_archive_link('online-exhibition') ?>" <?php echo $current_term->name === 'online-exhibition' ? 'selected' : '' ?>>Online</option>
        <?php foreach ($terms as $term) : $selected = $term == get_queried_object() ? 'selected' : ''; ?>
          <option value="<?php echo get_term_link( $term )  ?>" <?php echo $selected; ?> >
            <?php echo ucfirst($term->name) ?> Exhibitions
          </option>            
        <?php endforeach; ?>
      </select>
    </div>

    <div class="d-none d-lg-flex">      
      <ul>
        <li class="<?php echo $current_term->name === 'online-exhibition' ? 'active' : '' ?>">
          <a href="<?php echo get_post_type_archive_link('online-exhibition') ?>">Online</a>
        </li>
        <?php foreach ($terms as $term) : ?>
          <li class="<?php echo $term == $current_term ? 'active' : ''; ?>">
            <a title="Checkout the <?php $term->name ?> exhibitions" href="<?php echo get_term_link( $term )  ?>">
              <?php echo $term->name ?>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
<?php  endif; ?>