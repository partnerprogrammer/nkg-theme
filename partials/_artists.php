<div class="s__artists py-5 container mb-5 mt-2 mt-xl-5">
  <?php $artists = get_artists(); ?>
  <?php $add_artists = get_additional_artists(); ?>
  
  <div class="row">

    <div class="col-12 mb-5">
      <?php if (count($artists) > 0): ?>
        <a title="Go to Artists page" href="<?php echo get_post_type_archive_link('artists'); ?>"><h2 class="section-title font-bold pb-3 mb-4">Artists</h2></a>
        <div class="artists-list slick-home-artists">
          <?php 
            foreach ($artists as $artist ) {
              $thumb = has_post_thumbnail($artist->ID) ? get_the_post_thumbnail_url( $artist->ID, 'large') : '';
              $caption = has_post_thumbnail($artist->ID) ? get_the_post_thumbnail_caption( $artist->ID ) : '';
              $href = get_permalink($artist->ID);
              echo '<div><a data-caption="'.$caption.'" data-thumb="'.$thumb.'" class="text-uppercase" href="'.$href.'">'.$artist->post_title.'</a></div>';
            }
          ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="col-12 pt-4">
      <?php if (count($add_artists) > 0): ?>
        <a title="Go to Artists page" href="<?php echo get_post_type_archive_link('artists'); ?>"><h2 class="section-title font-bold pb-3 mb-4">Additional Artists Exhibited</h2></a>
        <div class="artists-list slick-home-add-artists">
          <?php 
            foreach ($add_artists as $artist ) {
              echo '<div><a class="text-uppercase" href="'.get_permalink($artist->ID).'">'.$artist->post_title.'</a></div>';
            }
          ?>
        </div>
      <?php endif; ?>
    </div>
  </div>


</div>