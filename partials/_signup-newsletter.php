<div class="block block-newsletter">
  <div class="container">
    <?php if (is_front_page()) : ?>
    <div class="row">
      <div class="col-lg-8 d-flex align-items-center mb-5">
        <h2 class="block-title font-semi mb-0 js-toggle-newsletter cursor-pointer"><span class="font-semi">Join</span> our mailing list for updates</h2>
        <!-- <div class="ml-5 mt-2"><button class="js-toggle-newsletter">Sign up</button></div> -->
      </div>
    </div>
    <?php else: ?>
    <div class="row">
      <div class="col-lg-8 d-flex align-items-center mb-5">
        <h2 class="block-title font-semi mb-0 js-toggle-newsletter cursor-pointer">Join our mailing list for updates</h2>
        <div class="ml-5 mt-2"><button class="js-toggle-newsletter">Sign up</button></div>
      </div>
    </div>
    <?php endif; ?>
    <div class="d-none js-toggle-newsletter-wrapper">
      <?php echo do_shortcode( '[contact-form-7 id="5058" title="Mailing"]') ?>
    </div>
  </div>
</div>