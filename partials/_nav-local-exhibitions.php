<?php 
  $artists = get_field('expo_artist');
  $press = get_field('p_expo');
  $tour_3d = get_field('3d_tour');
  $galleries = get_galleries_by_exhibition(get_the_ID());
  $pubs = get_publications_by(get_the_ID());
  
  $sections = [];

  if ($artists) {
    $sections['#artists'] = 'About the Artist';
  }
  if ($press) {
    $sections['#press'] = 'Press';
  }
  if ($tour_3d) {
    $sections['#tour'] = '3D Tour';
  }
  if ($galleries) {
    $sections['#images'] = 'Images';
  }
  if (has_video_in_galleries($galleries)) {
    $sections['#video'] = 'Video';
  }
  if ($pubs) {
    $sections['#publications'] = 'Publications';
  }

  if (count($sections) < 3) return ; 
?>
  <div id="nav-local-exhibition" class="navigation-local mb-5">
        
    <div class="d-lg-none">      
      <select class="js-navigate-onchange">
        <?php foreach ($sections as $anchor => $label) : ?>
          <option value="<?php echo $anchor;  ?>">
            <?php echo $anchor; ?>
          </option>            
        <?php endforeach; ?>
      </select>
    </div>

    <div class="d-none d-lg-flex">      
      <ul>
        <?php foreach ($sections as $anchor => $label) : ?>
          <li class="">
            <a title="Go to <?php echo $label ?>" href="<?php echo $anchor; ?>">
              <?php echo $label; ?>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>