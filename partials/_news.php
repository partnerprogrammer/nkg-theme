<?php
  $loop = new WP_Query( ['post_type' => 'post', 'posts_per_page' => 5 ] );
  if ( $loop->have_posts() ) :
?>



<div class="s__news">
  <div class="container">
    <div class="block-news">
      <div class="block-header border-0">        
        <a title="Go to Artists page" href="<?php echo get_post_type_archive_link('post'); ?>"><h2 class="section-title font-bold border-0">News</h2></a>
        <div class="block-header-navigation"></div>
      </div>
    </div>
  </div>

  <!-- <div class="container">
    <div class="row">
      <div class="col-12 d-flex justify-content-between align-items-center mb-3 mb-lg-5">
        <h2 class="section-title font-bold border-0 p-0 m-0">News</h2>
        <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="more pink font-24">See all</a>
      </div>
    </div>
  </div> -->

  <div class="slider-news">
    <?php
      while ( $loop->have_posts() ) : $loop->the_post();
        echo '<div class="d-flex">';
          _loop('loop-index');
        echo '</div>';
      endwhile;
    ?>
  </div>
</div>
<?php wp_reset_postdata(); endif; ?>