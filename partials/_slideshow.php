<?php
  $images = get_field('images');
  $hasArrow = count($images) > 1 ? 'has-arrows' : 'no-arrows';

  $fe = get_field('featured_exhibition');
  if ($images) {
    echo '<div id="fe-home-wrapper">';
      // echo '<div class="container">';
        echo '<div class="slick"  data-href="'.get_permalink($fe).'" id="fe-home">';
          foreach ($images as $img ) {
            echo '<div>',
                    '<div class="slick-content">',
                      '<img class="d-block" src="'.$img['url'].'" alt="'.$img['alt'].'">',
                    '</div>',
                  '</div>';
          }
        echo '</div>';
      // echo '</div>';
      echo '<div id="fe-home-navigation" class="'.$hasArrow.'">';
        if ($fe) {
          echo '<a class="content" href="'.get_permalink($fe).'" title="View exhibition">';
            $is_current = get_post_meta( $fe, 'is_current_exhibition', true );              
            if (has_term('upcoming', 'exhibition_year', $fe)) {
              echo '<span>Upcoming exhibition</span>';
            } elseif ($is_current) {
              echo '<span>Current exhibition</span>';               
            } else {
              echo '<span>Past exhibition</span>';               
            }
            echo '<h1 class="text-uppercase font-bold">'.get_the_title($fe).' <br />'. get_field('exhibition_name', $fe).'</h1>';
            echo '<p>'.get_field('exhibition_date', $fe) .'</p>';
          echo '</a>';              
        }          
          echo '<div class="arrows"></div>';
      echo '</div>';
    echo '</div>';
  }
?>