<header class="page-title d-none d-lg-block">
  <h1>
    <?php 
      if (is_tax('exhibition_year')) {
        $term = get_queried_object();
        echo $term->name . ' Exhibitions';
      } elseif (is_archive()) {
        echo post_type_archive_title();
      } else {
        echo 'News';
      }
    ?>
  </h1>  
</header>