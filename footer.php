<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
?>
	<?php _partial('_signup-newsletter'); ?>
	<footer class="main-footer" role="contentinfo">		
		<div class="container text-center">
			<div class="logo-footer">
				<a class="logo d-inline-block" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<h3 class="text-uppercase"><?php bloginfo('name'); ?></h3>
				</a>
			</div>
			<div class="infos">
				<?php if (get_field('address', 'option')) : ?>
					<p><?php the_field('address', 'option') ?></p>
				<?php endif; ?>
				
				<?php if (get_field('hours', 'option')) : ?>
					<p><?php the_field('hours', 'option') ?></p>
				<?php endif; ?>
				
				<?php if (get_field('telephone', 'option')) : ?>
					<p><?php the_field('telephone', 'option') ?></p>
				<?php endif; ?>
				
				<?php /* if (get_field('email', 'option')) : ?>
					<p><a class="pink" target="_blank" href="mailto:<?php the_field('email', 'option') ?>" rel="nofollow" title="Contact us"><?php the_field('email', 'option') ?></a></p>
				<?php endif; */ ?>
				
				<?php if (get_field('instagram', 'option')) : ?>
					<p class="mt-3 mb-5 my-md-0"><a class="pink" target="_blank" href="<?php the_field('instagram', 'option') ?>" rel="nofollow noreferrer" title="Follow us at instagram">Instagram</a></p>
				<?php endif; ?>
				
			</div>
		</div>		
	</footer>

<?php wp_footer(); ?>

<script defer>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-53112900-1', 'auto');
	ga('send', 'pageview');

</script>

</body>
</html>