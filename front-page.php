<?php
  get_header();

    _partial( '_wrap-start' );

      _partial( '_slideshow' );
      _partial( '_artists' );
      _partial( '_news' );
      
    _partial( '_wrap-end' );

  get_footer();